﻿namespace Strategy.Client
{
	using System;
	using Abstract;
	using Implement;

	class Program
	{
		static void Main(string[] args) {
			IProgrammerWork developingWork = new DevelopingWork();
			IProgrammerWork testingWork = new TestingWork();
			Programmer middle = new Programmer(developingWork);
			Console.WriteLine(middle.GetActivity());
			Console.WriteLine("Tester quit. Please, test feature!");
			middle.ProgrammerWork = testingWork;
			Console.WriteLine(middle.GetActivity());
			Console.ReadKey();
		}
	}
}
