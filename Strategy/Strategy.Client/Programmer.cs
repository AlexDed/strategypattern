﻿namespace Strategy.Client
{
	using Abstract;

	/// <summary>
	/// Type, that can change strategy of behaviour.
	/// </summary>
	public class Programmer
	{
		public IProgrammerWork ProgrammerWork { get; set; }

		public Programmer(IProgrammerWork programmerWork) {
			ProgrammerWork = programmerWork;
		}

		public string GetActivity() {
			return ProgrammerWork.DoWork();
		}
	}
}