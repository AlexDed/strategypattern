﻿namespace Strategy.Implement
{
	using Abstract;

	/// <summary>
	/// Implementing <see cref="IProgrammerWork"/>. Case of strategy.
	/// </summary>
	public class DevelopingWork : IProgrammerWork
	{
		public string DoWork() {
			return "I'm creating new feature.";
		}
	}
}