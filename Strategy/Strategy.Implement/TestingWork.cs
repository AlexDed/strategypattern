﻿namespace Strategy.Implement
{
	using Abstract;

	/// <summary>
	/// Implementing <see cref="IProgrammerWork"/>. Case of strategy.
	/// </summary>
	public class TestingWork : IProgrammerWork
	{
		public string DoWork() {
			return @"I'm testing code now.";
		}
	}
}