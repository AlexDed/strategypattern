﻿namespace Strategy.Abstract
{
	/// <summary>
	/// Abstract interface, described programmer.
	/// Abstract strategy interface.
	/// </summary>
	public interface IProgrammerWork
	{
		string DoWork();
	}
}